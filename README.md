# Elite Academy Antwerp React App
This is a web app that was started for EA Antwerp but was abandoned because of covid.

The project is origninaly hosted on a private repo from 2dot5makers but for the sake of demonstration it temporarily lives here.

This is the mockup we were workig towards [figma](https://www.figma.com/file/094tLGz6AObGnBnnVHZlBl/Elite-Academy)

# Run
To start the app run `npm run start`

