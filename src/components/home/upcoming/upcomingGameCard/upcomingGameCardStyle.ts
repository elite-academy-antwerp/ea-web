import { theme } from "@style/theme";
import styled from "styled-components";

export const UpcomingGameCardStyle = styled.div`
	background-color: ${theme.secondary};
	color: ${theme.light};
	font-family: ${theme.fontFamily};
	font-size: 18px;
	padding: 16px;
	border-radius: 4px;
	margin-bottom: 12px;

	box-shadow: 0 1px 3px hsla(0, 0%, 0%, 0.2);
`;

export const Opponent = styled.div`
	text-transform: uppercase;
	font-weight: 600;
	padding-left: 5px;
`;

export const Row = styled.div`
	display: flex;
	margin-bottom: 5px;
`;

export const BottomRow = styled.div`
	display: flex;
	font-size: 12px;
	justify-content: space-between;
	.anticon {
		margin-right: 5px;
	}
`;
