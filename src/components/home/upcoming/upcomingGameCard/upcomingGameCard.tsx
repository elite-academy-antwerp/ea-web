import * as React from "react";
import {
	Opponent,
	Row,
	UpcomingGameCardStyle,
	BottomRow
} from "@components/home/upcoming/upcomingGameCard/upcomingGameCardStyle";
import { Icon } from "antd";

export interface UpcomingGameCardProps {
	data: any;
}

export const UpcomingGameCard: React.StatelessComponent<UpcomingGameCardProps> = (
	props: UpcomingGameCardProps
) => {
	const { home, opponent, location, time, date } = props.data;

	return (
		<UpcomingGameCardStyle>
			<div>
				<Row>
					{home ? "vs " : "at "}
					<Opponent>{opponent}</Opponent>
				</Row>
				<BottomRow>
					<div>
						<Icon type="pushpin" />
						{location}
					</div>
					<div>
						<Icon type="clock-circle" />
						{time}
					</div>
					<div>
						<Icon type="calendar" />
						{date}
					</div>
				</BottomRow>
			</div>
		</UpcomingGameCardStyle>
	);
};
