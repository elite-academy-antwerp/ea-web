import * as React from "react";
import { useGetUpcomingGameForTeamQuery } from "generated/graphql";
import { UpcomingGameCard } from "../upcomingGameCard/upcomingGameCard";

export interface UpcomingGamesListProps {
	teamId: string;
}

export const UpcomingGamesList: React.FC<UpcomingGamesListProps> = props => {
	const { data, loading, error } = useGetUpcomingGameForTeamQuery({
		variables: {
			teamId: props.teamId
		}
	});

	if (loading) {
		return <p>Loading...</p>;
	}
	if (error) {
		console.log(error.message);

		return <p>Something went wrong</p>;
	}

	return (
		<div>
			{(data?.upcomingGameForTeam || []).map(upcomingGame => (
				<UpcomingGameCard data={upcomingGame} />
			))}
		</div>
	);
};
