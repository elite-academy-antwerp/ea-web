import React, { FC } from "react";
import { useGetPracticesByTeamQuery } from "generated/graphql";
import { PracticeElement } from "../practiceElement/practiceElement";

export interface PracticeListProps {
	teamId: string;
}

export const PracticeList: FC<PracticeListProps> = (props: PracticeListProps) => {
	const { data, loading, error } = useGetPracticesByTeamQuery({
		variables: { teamId: props.teamId }
	});

	if (loading) {
		return <p>Loading...</p>;
	}
	if (error) {
		console.log(error.message);

		return <p>Something went wrong</p>;
	}

	return (
		<div>
			{(data?.practicesByTeam || []).map(practice => (
				<PracticeElement key={practice.id} practice={practice} />
			))}
		</div>
	);
};
