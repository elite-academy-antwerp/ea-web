import { theme } from "@style/theme";
import styled from "styled-components";

export const PracticeElementStyle = styled.div`
	/* border-bottom: 1px solid #E3E3E3; */
	box-shadow: 0 1px 3px hsla(0, 0%, 0%, 0.2);
	padding: 16px;
	margin-bottom: 10px;
	border-radius: 4px;
	cursor: pointer;
`;

export const DateAndTime = styled.div`
	display: flex;
	align-items: baseline;
	margin-bottom: 10px;
`;

export const FormatDate = styled.div`
	color: ${theme.primary};
	font-family: ${theme.fontFamily};
	font-weight: 600;
	font-size: 20px;
	margin-right: 5px;
	text-transform: uppercase;
`;

export const Time = styled.div`
	color: ${theme.secondary};
`;

export const BottomRowWrapper = styled.div`
	display: flex;
	justify-content: space-between;
`;

export const ItemWrapper = styled.div`
	width: 300px;
`;

export const ItemLabel = styled.div`
	color: ${theme.lightGray};
	font-size: 10px;
`;

export const ItemValue = styled.div``;
