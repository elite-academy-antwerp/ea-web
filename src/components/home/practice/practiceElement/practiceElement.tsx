import * as React from "react";
import {
	BottomRowWrapper,
	DateAndTime,
	ItemLabel,
	ItemValue,
	ItemWrapper,
	PracticeElementStyle,
	Time,
	FormatDate
} from "@components/home/practice/practiceElement/practiceElementStyle";
import format from "date-fns/format";

export interface PracticeElementEntity {
	start: string;
	end: string;
	date: string;
	location: string;
	type: string;
}

export interface PracticeElementProps {
	practice: PracticeElementEntity;
}

export const PracticeElement: React.FC<PracticeElementProps> = (props: PracticeElementProps) => {
	const { start, end, date, location, type } = props.practice;

	const formattedDate = format(new Date(date), "eee MMM do");

	return (
		<PracticeElementStyle>
			<DateAndTime>
				<FormatDate>{formattedDate}</FormatDate>
				<Time>
					{start} - {end}
				</Time>
			</DateAndTime>
			<BottomRowWrapper>
				<ItemWrapper>
					<ItemLabel>Type</ItemLabel>
					<ItemValue>{type}</ItemValue>
				</ItemWrapper>
				<ItemWrapper>
					<ItemLabel>Location</ItemLabel>
					<ItemValue>{location}</ItemValue>
				</ItemWrapper>
			</BottomRowWrapper>
		</PracticeElementStyle>
	);
};
