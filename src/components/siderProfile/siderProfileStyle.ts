import styled from "styled-components";

export const SiderProfileStyle = styled.div`
	margin-top: 30px;
	margin-bottom: 30px;
	display: flex;
	margin-left: 24px;
`;

export const AvatarWrapper = styled.div`
	width: 40px;
`;

export const UserInfoWrapper = styled.div`
	width: 40px;
	width: calc(100% - 40px - 10px);
	margin-left: 10px;
`;
