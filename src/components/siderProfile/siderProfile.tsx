import * as React from "react";
import {
	SiderProfileStyle,
	AvatarWrapper,
	UserInfoWrapper
} from "@components/siderProfile/siderProfileStyle";
import { Avatar } from "antd";
import { useAuthContext } from "hooks/useAuthContext";

export interface SiderProfileProps {}

export const SiderProfile: React.StatelessComponent<SiderProfileProps> = (
	props: SiderProfileProps
) => {
	const {
		authState: { user }
	} = useAuthContext();

	return (
		<SiderProfileStyle>
			<AvatarWrapper>
				<Avatar size="large" icon="user" />
			</AvatarWrapper>
			<UserInfoWrapper>
				{/* <div>{user?.username}</div> */}
				<div>{user?.email}</div>
			</UserInfoWrapper>
		</SiderProfileStyle>
	);
};
