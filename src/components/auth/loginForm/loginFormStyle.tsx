import { theme } from "@style/theme";
import styled from "styled-components";

export const LoginFormStyle = styled.div`
	max-width: 500px;
	width: 100%;

	.ant-form-item-label > label {
		color: ${theme.secondary};
	}
`;
