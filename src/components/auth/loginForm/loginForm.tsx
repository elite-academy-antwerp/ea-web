import { requiredRule } from "@utils/rules/requiredRule";
import { Button, Form, Icon, Input } from "antd";
import { WrappedFormUtils } from "antd/lib/form/Form";
import * as React from "react";
import { LoginFormStyle } from "./loginFormStyle";

export interface LoginFormProps {
	form: WrappedFormUtils;
	handleSubmit(e: React.FormEvent): void;
}

export interface LoginFromValues {
	email: string;
	password: string;
}

export const LoginForm: React.StatelessComponent<LoginFormProps> = props => {
	const {
		form: { getFieldDecorator },
		handleSubmit
	} = props;

	return (
		<LoginFormStyle>
			<Form onSubmit={handleSubmit} layout="vertical" className="login-form">
				<Form.Item label="E-mail">
					{getFieldDecorator("email", {
						rules: [requiredRule]
					})(
						<Input
							size="large"
							prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
							// tslint:disable-next-line: no-any
						/>
					)}
				</Form.Item>
				<Form.Item label="Password">
					{getFieldDecorator("password", {
						rules: [requiredRule]
					})(
						<Input
							size="large"
							prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
							type="password"
						/>
					)}
				</Form.Item>
				<Form.Item>
					<Button size="large" htmlType="submit" block={true}>
						LOGIN
					</Button>
				</Form.Item>
			</Form>
		</LoginFormStyle>
	);
};
