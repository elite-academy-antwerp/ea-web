import { FC } from "react";
import { useAuthContext } from "hooks/useAuthContext";

export const Logout: FC = () => {
	// eslint-disable-next-line react-hooks/rules-of-hooks
	const { logout } = useAuthContext();

	logout();

	return null;
};
