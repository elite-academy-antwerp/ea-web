import { MainLayout } from "@components/main/mainLayout/mainLayout";
import { AuthState } from "@store/reducers/authReducer";
import { compose } from "redux";
import { AuthContextStateProps, withAuthContext } from "hooks/useAuthContext";
import { ReactNode } from "react";

export interface MainLayoutContainerProps {
	dark?: boolean;
	sidenav?: boolean;
	header?: boolean;
	contained?: boolean;
	renderItems?(isAuthenticated: boolean, user?: AuthState["user"]): NavItem[];
}

export interface NavItem {
	to: string;
	shouldRender: boolean;
	name: string | ReactNode;
	icon?: ReactNode;
	exact?: boolean;
	component?: ReactNode;
}

export const MainLayoutContainer = compose<React.ComponentType<MainLayoutContainerProps>>(
	withAuthContext
)(MainLayout);

export type MainLayoutProps = MainLayoutContainerProps & AuthContextStateProps;
