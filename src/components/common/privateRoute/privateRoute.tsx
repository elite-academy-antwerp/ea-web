import * as React from "react";
import { Redirect, Route, RouteProps } from "react-router";
import { useAuthContext } from "hooks/useAuthContext";

export interface PrivateRouteProps extends RouteProps {
	noRedirect?: boolean;
}

const PrivateRouteComponent: React.StatelessComponent<PrivateRouteProps> = ({
	component: Component,
	render,
	noRedirect,
	...rest
}) => {
	const {
		authState: { isAuthenticated }
	} = useAuthContext();
	const state = noRedirect ? {} : { redirectFrom: rest.location };

	return (
		<Route
			{...rest}
			render={props => {
				if (isAuthenticated) {
					if (Component) {
						return <Component {...props} />;
					} else if (render) {
						return render(props);
					}
				}

				return (
					<Redirect
						to={{
							pathname: "/login",
							state
						}}
					/>
				);
			}}
		/>
	);
};

export const PrivateRoute = PrivateRouteComponent;
