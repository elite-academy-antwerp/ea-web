import { gql } from "apollo-boost";
import { TEAM } from "./teamService";

export const GET_USERS = gql`
	query GetUsers {
		users {
			id
			email
			team {
				name
			}
			role
		}
	}
`;

export const GET_LOGGEDIN_USER = gql`
	query GetLoggedinUser {
		me {
			id
			email
			team {
				id
				name
			}
			role
		}
	}
`;

export const USER = gql`
	fragment User on User {
		id
		email
		role
		team {
			...Team
		}
	}
	${TEAM}
`;
