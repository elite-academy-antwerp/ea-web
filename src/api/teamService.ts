import { gql } from "apollo-boost";
import { USER } from "./userService";

export const GET_TEAMS = gql`
	query GetTeams {
		teams {
			id
			name
			coach {
				id
			}
			assistant {
				id
			}
		}
	}
`;

export const GET_TEAM = gql`
	query GetTeamById($id: String!) {
		team(id: $id) {
			id
			name
			coach {
				id
			}
			assistant {
				id
			}
		}
	}
`;

export const TEAM = gql`
	fragment Team on Team {
		__typename
		id
		name
		coach {
			id
			email
		}
		assistant {
			id
			email
		}
	}
` as any;
