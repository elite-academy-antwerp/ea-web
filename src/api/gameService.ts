import { gql } from "apollo-boost";
import { TEAM } from "./teamService";

export const GET_UPCOMING_GAME_FOR_TEAM = gql`
	query GetUpcomingGameForTeam($teamId: String!) {
		upcomingGameForTeam(teamId: $teamId) {
			id
			date
			time
			home
			location
			opponent
			team {
				...Team
			}
		}
	}
	${TEAM}
`;
