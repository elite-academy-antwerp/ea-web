import { AxiosPromise, AxiosInstance, AxiosRequestConfig } from "axios";
import Axios from "axios";

function setParam(distObject: any, key: string, param: any) {
	if (param !== undefined) distObject[key] = param;
}

export function createApi(axios: AxiosInstance = Axios.create({ baseURL: "" })): ApiInstance {
	return {
		$axios: axios
	};
}

interface Core {
	$axios: AxiosInstance;
}

export interface ApiInstance extends Core {}
