import { gql } from "apollo-boost";
import { TEAM } from "./teamService";

export const GET_PRACTICES = gql`
	query GetPractices {
		practices {
			id
			date
			start
			end
			location
			type
			team {
				name
			}
		}
	}
`;

export const GET_PRACTICE = gql`
	query GetPracticeById($id: String!) {
		practice(id: $id) {
			id
			date
			start
			end
			location
			type
			team {
				name
			}
		}
	}
`;

export const GET_PRACTICES_BY_TEAM = gql`
	query GetPracticesByTeam($teamId: String!) {
		practicesByTeam(teamId: $teamId) {
			id
			date
			start
			end
			location
			type
			team {
				...Team
			}
		}
	}
	${TEAM}
`;
