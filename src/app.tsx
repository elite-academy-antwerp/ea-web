import { PrivateRoute } from "@components/common/privateRoute/privateRoute";
import { MainLayoutContainer, NavItem } from "@components/main/mainLayout/mainLayoutContainer";
import { LoginContainer } from "@pages/auth/login/loginContainer";
import { configureStore } from "@store/configureStore";
import ApolloClient from "apollo-boost";
import * as React from "react";
import { addLocaleData, FormattedMessage, IntlProvider } from "react-intl";
import nlLocaleData from "react-intl/locale-data/nl";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { API_URL } from "@utils/env";
import { ApolloProvider } from "@apollo/react-hooks";
import { AuthProvider } from "hooks/useAuthContext";
import { Logout } from "@components/auth/logout";
import { Main } from "@pages/main/main";
require("typeface-open-sans");
require("typeface-oswald");

addLocaleData([...nlLocaleData]);

const messages = require("./translations/nl.json");

const { store } = configureStore({});

const renderNav = (isAuthenticated: boolean): NavItem[] => {
	return [
		{
			name: <FormattedMessage id="home" />,
			shouldRender: isAuthenticated,
			to: "/",
			exact: true
		},
		{
			name: <FormattedMessage id="games" />,
			shouldRender: isAuthenticated,
			to: "/games",
			exact: true
		},
		{
			name: <FormattedMessage id="practices" />,
			shouldRender: isAuthenticated,
			to: "/practices"
		},
		{
			name: <FormattedMessage id="team" />,
			shouldRender: isAuthenticated,
			to: "/team"
		},
		{
			name: <FormattedMessage id="logout" />,
			shouldRender: isAuthenticated,
			to: "/logout"
		}
	];
};

// const authLink = setContext((_, { headers }) => {
// 	// get the authentication token from local storage if it exists
// 	const token = localStorage.getItem('token');
// 	// return the headers to the context so httpLink can read them
// 	return {
// 		headers: {
// 			...headers,
// 			authorization: token ? `Bearer ${token}` : "",
// 		}
// 	}
// });
const token = localStorage.getItem("token");
const appolloClient = new ApolloClient({
	headers: {
		authorization: token ? `Bearer ${token}` : ""
	},
	uri: API_URL
});

export const App: React.FC = () => (
	<Provider store={store}>
		<IntlProvider messages={messages} locale="nl-BE">
			<ApolloProvider client={appolloClient}>
				<BrowserRouter>
					<AuthProvider>
						<Switch>
							<Route path="/login" component={LoginContainer} />
							<PrivateRoute noRedirect path="/logout" component={Logout} />
							<Route
								path="*"
								render={() => (
									<MainLayoutContainer renderItems={renderNav} sidenav>
										<PrivateRoute exact path="/" component={Main} />
									</MainLayoutContainer>
								)}
							/>
						</Switch>
					</AuthProvider>
				</BrowserRouter>
			</ApolloProvider>
		</IntlProvider>
	</Provider>
);
