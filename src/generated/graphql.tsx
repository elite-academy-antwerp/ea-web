import gql from "graphql-tag";
import * as ApolloReactCommon from "@apollo/react-common";
import * as ApolloReactHooks from "@apollo/react-hooks";
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
	ID: string;
	String: string;
	Boolean: boolean;
	Int: number;
	Float: number;
};

export type Game = {
	__typename?: "Game";
	id: Scalars["String"];
	opponent: Scalars["String"];
	location: Scalars["String"];
	time: Scalars["String"];
	date: Scalars["String"];
	home: Scalars["Boolean"];
	team: Team;
};

export type GameInput = {
	opponent: Scalars["String"];
	location: Scalars["String"];
	time: Scalars["String"];
	date: Scalars["String"];
	home: Scalars["Boolean"];
	teamId: Scalars["String"];
};

export type Mutation = {
	__typename?: "Mutation";
	login?: Maybe<TokenResponse>;
	createGame?: Maybe<Game>;
	createPractice?: Maybe<Practice>;
	createTeam?: Maybe<Team>;
	createUser?: Maybe<User>;
};

export type MutationLoginArgs = {
	email: Scalars["String"];
	password: Scalars["String"];
};

export type MutationCreateGameArgs = {
	input: GameInput;
};

export type MutationCreatePracticeArgs = {
	input: PracticeInput;
};

export type MutationCreateTeamArgs = {
	input: TeamInput;
};

export type MutationCreateUserArgs = {
	input: UserInput;
};

export type Practice = {
	__typename?: "Practice";
	id: Scalars["String"];
	date: Scalars["String"];
	start: Scalars["String"];
	end: Scalars["String"];
	type: TrainingType;
	location: Scalars["String"];
	team: Team;
};

export type PracticeInput = {
	date: Scalars["String"];
	start: Scalars["String"];
	end: Scalars["String"];
	type: TrainingType;
	location: Scalars["String"];
	teamId: Scalars["String"];
};

export type Query = {
	__typename?: "Query";
	games?: Maybe<Array<Game>>;
	upcomingGameForTeam?: Maybe<Array<Game>>;
	game: Game;
	practices?: Maybe<Array<Practice>>;
	practicesByTeam?: Maybe<Array<Practice>>;
	practice: Practice;
	teams?: Maybe<Array<Team>>;
	team: Team;
	hello: Scalars["String"];
	users?: Maybe<Array<User>>;
	me: User;
};

export type QueryUpcomingGameForTeamArgs = {
	teamId: Scalars["String"];
};

export type QueryGameArgs = {
	id: Scalars["String"];
};

export type QueryPracticesByTeamArgs = {
	teamId: Scalars["String"];
};

export type QueryPracticeArgs = {
	id: Scalars["String"];
};

export type QueryTeamArgs = {
	id: Scalars["String"];
};

export enum Role {
	Admin = "ADMIN",
	Player = "PLAYER",
	Coach = "COACH"
}

export type Team = {
	__typename?: "Team";
	id: Scalars["String"];
	name: Scalars["String"];
	coach: User;
	assistant: User;
};

export type TeamInput = {
	name: Scalars["String"];
	coachId: Scalars["String"];
	assistantId: Scalars["String"];
};

export type TokenResponse = {
	__typename?: "TokenResponse";
	id_token: Scalars["String"];
};

export enum TrainingType {
	Skill = "SKILL",
	Strength = "STRENGTH",
	Team = "TEAM"
}

export type User = {
	__typename?: "User";
	id: Scalars["String"];
	email: Scalars["String"];
	team: Team;
	role: Role;
};

export type UserInput = {
	email: Scalars["String"];
	password: Scalars["String"];
	teamId: Scalars["String"];
	role: Role;
};

export type GetUpcomingGameForTeamQueryVariables = {
	teamId: Scalars["String"];
};

export type GetUpcomingGameForTeamQuery = { __typename?: "Query" } & {
	upcomingGameForTeam: Maybe<
		Array<
			{ __typename?: "Game" } & Pick<
				Game,
				"id" | "date" | "time" | "home" | "location" | "opponent"
			> & { team: { __typename?: "Team" } & TeamFragment }
		>
	>;
};

export type GetPracticesQueryVariables = {};

export type GetPracticesQuery = { __typename?: "Query" } & {
	practices: Maybe<
		Array<
			{ __typename?: "Practice" } & Pick<
				Practice,
				"id" | "date" | "start" | "end" | "location" | "type"
			> & { team: { __typename?: "Team" } & Pick<Team, "name"> }
		>
	>;
};

export type GetPracticeByIdQueryVariables = {
	id: Scalars["String"];
};

export type GetPracticeByIdQuery = { __typename?: "Query" } & {
	practice: { __typename?: "Practice" } & Pick<
		Practice,
		"id" | "date" | "start" | "end" | "location" | "type"
	> & { team: { __typename?: "Team" } & Pick<Team, "name"> };
};

export type GetPracticesByTeamQueryVariables = {
	teamId: Scalars["String"];
};

export type GetPracticesByTeamQuery = { __typename?: "Query" } & {
	practicesByTeam: Maybe<
		Array<
			{ __typename?: "Practice" } & Pick<
				Practice,
				"id" | "date" | "start" | "end" | "location" | "type"
			> & { team: { __typename?: "Team" } & TeamFragment }
		>
	>;
};

export type GetTeamsQueryVariables = {};

export type GetTeamsQuery = { __typename?: "Query" } & {
	teams: Maybe<
		Array<
			{ __typename?: "Team" } & Pick<Team, "id" | "name"> & {
					coach: { __typename?: "User" } & Pick<User, "id">;
					assistant: { __typename?: "User" } & Pick<User, "id">;
				}
		>
	>;
};

export type GetTeamByIdQueryVariables = {
	id: Scalars["String"];
};

export type GetTeamByIdQuery = { __typename?: "Query" } & {
	team: { __typename?: "Team" } & Pick<Team, "id" | "name"> & {
			coach: { __typename?: "User" } & Pick<User, "id">;
			assistant: { __typename?: "User" } & Pick<User, "id">;
		};
};

export type TeamFragment = { __typename: "Team" } & Pick<Team, "id" | "name"> & {
		coach: { __typename?: "User" } & Pick<User, "id" | "email">;
		assistant: { __typename?: "User" } & Pick<User, "id" | "email">;
	};

export type GetUsersQueryVariables = {};

export type GetUsersQuery = { __typename?: "Query" } & {
	users: Maybe<
		Array<
			{ __typename?: "User" } & Pick<User, "id" | "email" | "role"> & {
					team: { __typename?: "Team" } & Pick<Team, "name">;
				}
		>
	>;
};

export type GetLoggedinUserQueryVariables = {};

export type GetLoggedinUserQuery = { __typename?: "Query" } & {
	me: { __typename?: "User" } & Pick<User, "id" | "email" | "role"> & {
			team: { __typename?: "Team" } & Pick<Team, "id" | "name">;
		};
};

export type UserFragment = { __typename?: "User" } & Pick<User, "id" | "email" | "role"> & {
		team: { __typename?: "Team" } & TeamFragment;
	};

export type LoginMutationVariables = {
	email: Scalars["String"];
	password: Scalars["String"];
};

export type LoginMutation = { __typename?: "Mutation" } & {
	login: Maybe<{ __typename?: "TokenResponse" } & Pick<TokenResponse, "id_token">>;
};

export const TeamFragmentDoc = gql`
	fragment Team on Team {
		__typename
		id
		name
		coach {
			id
			email
		}
		assistant {
			id
			email
		}
	}
`;
export const UserFragmentDoc = gql`
	fragment User on User {
		id
		email
		role
		team {
			...Team
		}
	}
	${TeamFragmentDoc}
`;
export const GetUpcomingGameForTeamDocument = gql`
	query GetUpcomingGameForTeam($teamId: String!) {
		upcomingGameForTeam(teamId: $teamId) {
			id
			date
			time
			home
			location
			opponent
			team {
				...Team
			}
		}
	}
	${TeamFragmentDoc}
`;

/**
 * __useGetUpcomingGameForTeamQuery__
 *
 * To run a query within a React component, call `useGetUpcomingGameForTeamQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUpcomingGameForTeamQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUpcomingGameForTeamQuery({
 *   variables: {
 *      teamId: // value for 'teamId'
 *   },
 * });
 */
export function useGetUpcomingGameForTeamQuery(
	baseOptions?: ApolloReactHooks.QueryHookOptions<
		GetUpcomingGameForTeamQuery,
		GetUpcomingGameForTeamQueryVariables
	>
) {
	return ApolloReactHooks.useQuery<
		GetUpcomingGameForTeamQuery,
		GetUpcomingGameForTeamQueryVariables
	>(GetUpcomingGameForTeamDocument, baseOptions);
}
export function useGetUpcomingGameForTeamLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
		GetUpcomingGameForTeamQuery,
		GetUpcomingGameForTeamQueryVariables
	>
) {
	return ApolloReactHooks.useLazyQuery<
		GetUpcomingGameForTeamQuery,
		GetUpcomingGameForTeamQueryVariables
	>(GetUpcomingGameForTeamDocument, baseOptions);
}
export type GetUpcomingGameForTeamQueryHookResult = ReturnType<
	typeof useGetUpcomingGameForTeamQuery
>;
export type GetUpcomingGameForTeamLazyQueryHookResult = ReturnType<
	typeof useGetUpcomingGameForTeamLazyQuery
>;
export type GetUpcomingGameForTeamQueryResult = ApolloReactCommon.QueryResult<
	GetUpcomingGameForTeamQuery,
	GetUpcomingGameForTeamQueryVariables
>;
export const GetPracticesDocument = gql`
	query GetPractices {
		practices {
			id
			date
			start
			end
			location
			type
			team {
				name
			}
		}
	}
`;

/**
 * __useGetPracticesQuery__
 *
 * To run a query within a React component, call `useGetPracticesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPracticesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPracticesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetPracticesQuery(
	baseOptions?: ApolloReactHooks.QueryHookOptions<GetPracticesQuery, GetPracticesQueryVariables>
) {
	return ApolloReactHooks.useQuery<GetPracticesQuery, GetPracticesQueryVariables>(
		GetPracticesDocument,
		baseOptions
	);
}
export function useGetPracticesLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
		GetPracticesQuery,
		GetPracticesQueryVariables
	>
) {
	return ApolloReactHooks.useLazyQuery<GetPracticesQuery, GetPracticesQueryVariables>(
		GetPracticesDocument,
		baseOptions
	);
}
export type GetPracticesQueryHookResult = ReturnType<typeof useGetPracticesQuery>;
export type GetPracticesLazyQueryHookResult = ReturnType<typeof useGetPracticesLazyQuery>;
export type GetPracticesQueryResult = ApolloReactCommon.QueryResult<
	GetPracticesQuery,
	GetPracticesQueryVariables
>;
export const GetPracticeByIdDocument = gql`
	query GetPracticeById($id: String!) {
		practice(id: $id) {
			id
			date
			start
			end
			location
			type
			team {
				name
			}
		}
	}
`;

/**
 * __useGetPracticeByIdQuery__
 *
 * To run a query within a React component, call `useGetPracticeByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPracticeByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPracticeByIdQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetPracticeByIdQuery(
	baseOptions?: ApolloReactHooks.QueryHookOptions<
		GetPracticeByIdQuery,
		GetPracticeByIdQueryVariables
	>
) {
	return ApolloReactHooks.useQuery<GetPracticeByIdQuery, GetPracticeByIdQueryVariables>(
		GetPracticeByIdDocument,
		baseOptions
	);
}
export function useGetPracticeByIdLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
		GetPracticeByIdQuery,
		GetPracticeByIdQueryVariables
	>
) {
	return ApolloReactHooks.useLazyQuery<GetPracticeByIdQuery, GetPracticeByIdQueryVariables>(
		GetPracticeByIdDocument,
		baseOptions
	);
}
export type GetPracticeByIdQueryHookResult = ReturnType<typeof useGetPracticeByIdQuery>;
export type GetPracticeByIdLazyQueryHookResult = ReturnType<typeof useGetPracticeByIdLazyQuery>;
export type GetPracticeByIdQueryResult = ApolloReactCommon.QueryResult<
	GetPracticeByIdQuery,
	GetPracticeByIdQueryVariables
>;
export const GetPracticesByTeamDocument = gql`
	query GetPracticesByTeam($teamId: String!) {
		practicesByTeam(teamId: $teamId) {
			id
			date
			start
			end
			location
			type
			team {
				...Team
			}
		}
	}
	${TeamFragmentDoc}
`;

/**
 * __useGetPracticesByTeamQuery__
 *
 * To run a query within a React component, call `useGetPracticesByTeamQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPracticesByTeamQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPracticesByTeamQuery({
 *   variables: {
 *      teamId: // value for 'teamId'
 *   },
 * });
 */
export function useGetPracticesByTeamQuery(
	baseOptions?: ApolloReactHooks.QueryHookOptions<
		GetPracticesByTeamQuery,
		GetPracticesByTeamQueryVariables
	>
) {
	return ApolloReactHooks.useQuery<GetPracticesByTeamQuery, GetPracticesByTeamQueryVariables>(
		GetPracticesByTeamDocument,
		baseOptions
	);
}
export function useGetPracticesByTeamLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
		GetPracticesByTeamQuery,
		GetPracticesByTeamQueryVariables
	>
) {
	return ApolloReactHooks.useLazyQuery<GetPracticesByTeamQuery, GetPracticesByTeamQueryVariables>(
		GetPracticesByTeamDocument,
		baseOptions
	);
}
export type GetPracticesByTeamQueryHookResult = ReturnType<typeof useGetPracticesByTeamQuery>;
export type GetPracticesByTeamLazyQueryHookResult = ReturnType<
	typeof useGetPracticesByTeamLazyQuery
>;
export type GetPracticesByTeamQueryResult = ApolloReactCommon.QueryResult<
	GetPracticesByTeamQuery,
	GetPracticesByTeamQueryVariables
>;
export const GetTeamsDocument = gql`
	query GetTeams {
		teams {
			id
			name
			coach {
				id
			}
			assistant {
				id
			}
		}
	}
`;

/**
 * __useGetTeamsQuery__
 *
 * To run a query within a React component, call `useGetTeamsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetTeamsQuery(
	baseOptions?: ApolloReactHooks.QueryHookOptions<GetTeamsQuery, GetTeamsQueryVariables>
) {
	return ApolloReactHooks.useQuery<GetTeamsQuery, GetTeamsQueryVariables>(
		GetTeamsDocument,
		baseOptions
	);
}
export function useGetTeamsLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetTeamsQuery, GetTeamsQueryVariables>
) {
	return ApolloReactHooks.useLazyQuery<GetTeamsQuery, GetTeamsQueryVariables>(
		GetTeamsDocument,
		baseOptions
	);
}
export type GetTeamsQueryHookResult = ReturnType<typeof useGetTeamsQuery>;
export type GetTeamsLazyQueryHookResult = ReturnType<typeof useGetTeamsLazyQuery>;
export type GetTeamsQueryResult = ApolloReactCommon.QueryResult<
	GetTeamsQuery,
	GetTeamsQueryVariables
>;
export const GetTeamByIdDocument = gql`
	query GetTeamById($id: String!) {
		team(id: $id) {
			id
			name
			coach {
				id
			}
			assistant {
				id
			}
		}
	}
`;

/**
 * __useGetTeamByIdQuery__
 *
 * To run a query within a React component, call `useGetTeamByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTeamByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTeamByIdQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetTeamByIdQuery(
	baseOptions?: ApolloReactHooks.QueryHookOptions<GetTeamByIdQuery, GetTeamByIdQueryVariables>
) {
	return ApolloReactHooks.useQuery<GetTeamByIdQuery, GetTeamByIdQueryVariables>(
		GetTeamByIdDocument,
		baseOptions
	);
}
export function useGetTeamByIdLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetTeamByIdQuery, GetTeamByIdQueryVariables>
) {
	return ApolloReactHooks.useLazyQuery<GetTeamByIdQuery, GetTeamByIdQueryVariables>(
		GetTeamByIdDocument,
		baseOptions
	);
}
export type GetTeamByIdQueryHookResult = ReturnType<typeof useGetTeamByIdQuery>;
export type GetTeamByIdLazyQueryHookResult = ReturnType<typeof useGetTeamByIdLazyQuery>;
export type GetTeamByIdQueryResult = ApolloReactCommon.QueryResult<
	GetTeamByIdQuery,
	GetTeamByIdQueryVariables
>;
export const GetUsersDocument = gql`
	query GetUsers {
		users {
			id
			email
			team {
				name
			}
			role
		}
	}
`;

/**
 * __useGetUsersQuery__
 *
 * To run a query within a React component, call `useGetUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUsersQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetUsersQuery(
	baseOptions?: ApolloReactHooks.QueryHookOptions<GetUsersQuery, GetUsersQueryVariables>
) {
	return ApolloReactHooks.useQuery<GetUsersQuery, GetUsersQueryVariables>(
		GetUsersDocument,
		baseOptions
	);
}
export function useGetUsersLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetUsersQuery, GetUsersQueryVariables>
) {
	return ApolloReactHooks.useLazyQuery<GetUsersQuery, GetUsersQueryVariables>(
		GetUsersDocument,
		baseOptions
	);
}
export type GetUsersQueryHookResult = ReturnType<typeof useGetUsersQuery>;
export type GetUsersLazyQueryHookResult = ReturnType<typeof useGetUsersLazyQuery>;
export type GetUsersQueryResult = ApolloReactCommon.QueryResult<
	GetUsersQuery,
	GetUsersQueryVariables
>;
export const GetLoggedinUserDocument = gql`
	query GetLoggedinUser {
		me {
			id
			email
			team {
				id
				name
			}
			role
		}
	}
`;

/**
 * __useGetLoggedinUserQuery__
 *
 * To run a query within a React component, call `useGetLoggedinUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetLoggedinUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetLoggedinUserQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetLoggedinUserQuery(
	baseOptions?: ApolloReactHooks.QueryHookOptions<
		GetLoggedinUserQuery,
		GetLoggedinUserQueryVariables
	>
) {
	return ApolloReactHooks.useQuery<GetLoggedinUserQuery, GetLoggedinUserQueryVariables>(
		GetLoggedinUserDocument,
		baseOptions
	);
}
export function useGetLoggedinUserLazyQuery(
	baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
		GetLoggedinUserQuery,
		GetLoggedinUserQueryVariables
	>
) {
	return ApolloReactHooks.useLazyQuery<GetLoggedinUserQuery, GetLoggedinUserQueryVariables>(
		GetLoggedinUserDocument,
		baseOptions
	);
}
export type GetLoggedinUserQueryHookResult = ReturnType<typeof useGetLoggedinUserQuery>;
export type GetLoggedinUserLazyQueryHookResult = ReturnType<typeof useGetLoggedinUserLazyQuery>;
export type GetLoggedinUserQueryResult = ApolloReactCommon.QueryResult<
	GetLoggedinUserQuery,
	GetLoggedinUserQueryVariables
>;
export const LoginDocument = gql`
	mutation Login($email: String!, $password: String!) {
		login(email: $email, password: $password) {
			id_token
		}
	}
`;
export type LoginMutationFn = ApolloReactCommon.MutationFunction<
	LoginMutation,
	LoginMutationVariables
>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(
	baseOptions?: ApolloReactHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>
) {
	return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(
		LoginDocument,
		baseOptions
	);
}
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = ApolloReactCommon.MutationResult<LoginMutation>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<
	LoginMutation,
	LoginMutationVariables
>;
