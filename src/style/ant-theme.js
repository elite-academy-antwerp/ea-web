// https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less

module.exports = {
	"blue-6": "#2D7CCE",
	"primary-color": "#590313",
	"border-radius-base": "4px",
	"layout-header-background": "#212121",
	"font-family": "Open Sans",
	"text-color": "@black",
	// BUTTONS
	"btn-default-color": "@white",
	"btn-default-bg": "#D9C091",
	"btn-default-border": "#D9C091",
	"btn-font-weight": "700",
	
	// Layout
	"layout-body-background": "@white"
};
