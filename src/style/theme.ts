export const theme = {
	blue6: "#2D7CCE",
	primary: "#590313",
	secondary: "#D9C091",
	fontFamily: "Oswald",
	fontFamilyHeader: "PT Sans, sans-serif",
	fontWeightBold: 500,
	dark: "#212121",
	light: "#fff",
	lightGray: "#AEAEAE"
};
