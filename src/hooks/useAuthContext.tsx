import React, { useContext, FC, useState, useEffect } from "react";
import { AuthService } from "@utils/auth/authService";
import { IdToken } from "@utils/auth/idToken";

interface Token extends IdToken {
	email: string;
	username: string;
	team: string;
	role: string;
}

export interface AuthContextState {
	isAuthenticated: boolean;
	user?: Token;
}

export interface AuthContextStateProps {
	authState: AuthContextState;
	login(token: string): void;
	logout(): void;
}

export const AuthContext = React.createContext<AuthContextStateProps>({
	authState: {
		isAuthenticated: false
	},
	// tslint:disable-next-line: typedef
	login(_) {
		throw new Error("Not implemented");
	},
	// tslint:disable-next-line: typedef
	logout() {
		throw new Error("Not implemented");
	}
});

export const useAuthContext = () => useContext(AuthContext);

export const AuthProvider: FC = ({ children }) => {
	const [authState, setAuthState] = useState<AuthContextState>({ isAuthenticated: false });

	useEffect(() => {
		const decodedToken = AuthService.getDecodedToken<Token>();

		if (decodedToken) {
			setAuthState({
				isAuthenticated: true,
				user: decodedToken
			});
		}
	}, []);

	const login = (token: string) => {
		const user = AuthService.getDecodedToken<Token>(token);

		if (token) {
			AuthService.storeJwt(token);
		}

		setAuthState({
			isAuthenticated: true,
			user
		});
	};

	const logout = () => {
		AuthService.logout();

		setAuthState({
			isAuthenticated: false,
			user: undefined
		});
	};

	return (
		<AuthContext.Provider
			value={{
				authState,
				login,
				logout
			}}>
			{children}
		</AuthContext.Provider>
	);
};

export const withAuthContext = <P extends object>(
	Component: React.ComponentType<P>
): React.FC<Omit<P, keyof AuthContextStateProps>> => props => (
	<AuthContext.Consumer>
		{context => <Component {...(props as P)} {...context} />}
	</AuthContext.Consumer>
);
