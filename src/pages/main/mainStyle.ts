import { theme } from "@style/theme";
import styled from "styled-components";

export const MainStyle = styled.div``;

export const Title = styled.h1`
	color: ${theme.primary};
	font-family: ${theme.fontFamily};

	font-size: 30px;
`;
