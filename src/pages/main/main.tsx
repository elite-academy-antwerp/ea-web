import { MainStyle, Title } from "@pages/main/mainStyle";
import * as React from "react";
import { PracticeList } from "@components/home/practice/practiceList/practiceList";
import { FC } from "react";
import { useAuthContext } from "hooks/useAuthContext";
import { UpcomingGamesList } from "@components/home/upcoming/upcomingGamesList/upcomingGamesList";

export interface MainProps {}

export const Main: FC<MainProps> = () => {
	const { authState } = useAuthContext();
	// const [{ id: teamId } = {}] = teams?.data?.teams ?? [];
	const teamId = authState?.user?.team as string;

	return (
		<MainStyle>
			<Title>Upcoming Games</Title>
			<UpcomingGamesList teamId={teamId} />
			<Title>Practices</Title>
			<PracticeList teamId={teamId} />
		</MainStyle>
	);
};
