import logo from "@assets/images/EA-LOGO.png";
import { LoginForm, LoginFromValues } from "@components/auth/loginForm/loginForm";
import { LoginProps } from "@pages/auth/login/loginContainer";
import { LoginStyle, Logo } from "@pages/auth/login/loginStyle";
import { Alert } from "antd";
import React, { useState } from "react";
import { Redirect } from "react-router";
import { gql } from "apollo-boost";
import { useLoginMutation } from "generated/graphql";
import { useAuthContext } from "hooks/useAuthContext";

interface State {
	error: boolean;
}

export const LOGIN = gql`
	mutation Login($email: String!, $password: String!) {
		login(email: $email, password: $password) {
			id_token
		}
	}
`;

export const Login: React.FC<LoginProps> = ({ intl, form, location: { state } }) => {
	const [hasError, setError] = useState(false);
	const [login] = useLoginMutation();
	const {
		authState: { isAuthenticated },
		...authContext
	} = useAuthContext();

	if (isAuthenticated) {
		const redirectTo = (state || {}).redirectFrom || "/";

		return <Redirect to={redirectTo} />;
	}

	const handleSubmit = (event: React.FormEvent) => {
		event.preventDefault();

		form.validateFields(async (errors, values: LoginFromValues) => {
			if (errors) {
				return;
			}

			try {
				const res = await login({
					variables: values
				});

				if (res.data?.login?.id_token) {
					authContext.login(res.data?.login?.id_token);
				} else {
					setError(true);
				}
			} catch (err) {
				setError(true);
				console.error(err);
			}
		});
	};

	return (
		<LoginStyle>
			<Logo src={logo} />

			{hasError && (
				<Alert message={intl.formatMessage({ id: "auth.login.failed" })} type="error" />
			)}

			<LoginForm form={form} handleSubmit={handleSubmit} />
		</LoginStyle>
	);
};
