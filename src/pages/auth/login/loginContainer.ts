import { Login } from "@pages/auth/login/login";
import { Form } from "antd";
import { FormComponentProps } from "antd/lib/form";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { RouteComponentProps } from "react-router";
import { compose } from "redux";

export interface LoginContainerProps {}

const withForm = Form.create();

export const LoginContainer = compose<React.ComponentType<LoginContainerProps>>(
	withForm,
	injectIntl
)(Login);

export type LoginProps = LoginContainerProps &
	FormComponentProps &
	InjectedIntlProps &
	RouteComponentProps<{}, {}, { redirectFrom: string }>;
