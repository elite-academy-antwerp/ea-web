import { theme } from "@style/theme";
import styled from "styled-components";

export const LoginStyle = styled.div`
	text-align: center;
	display: flex;
	flex-direction: column;
	align-items: center;
	background-color: ${theme.primary};
	height: 100%;
	padding: 10%;

	font-family: "Oswald";

	.ant-alert {
		margin-bottom: 2rem;
	}

	/* @media screen and (min-width: 700px) {
		grid-template-columns: 1fr 1.5fr;
		grid-template-areas: "sidebar image";
	}

	@media screen and (min-width: 900px) {
		grid-template-columns: 1fr 2.5fr;
		grid-template-areas: "sidebar image";
	} */
`;

export const Logo = styled.img`
	max-width: 200px;
	margin: 45px 0;
`;
