import { SiderProfile } from "@components/siderProfile/siderProfile";
import React from "react";
import renderer from "react-test-renderer";

it("should match snapshot", () => {
	const wrapper = renderer.create(<SiderProfile />);
	const tree = wrapper.toJSON();
	expect(tree).toMatchSnapshot();
});
