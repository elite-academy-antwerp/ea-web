import { PracticeElement } from "@components/home/practice/practiceElement/practiceElement";
import React from "react";
import renderer from "react-test-renderer";

it("should match snapshot", () => {
	const wrapper = renderer.create(<PracticeElement />);
	const tree = wrapper.toJSON();
	expect(tree).toMatchSnapshot();
});
