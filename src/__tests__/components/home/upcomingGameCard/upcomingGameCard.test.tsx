import { UpcomingGameCard } from "@components/home/upcomingGameCard/upcomingGameCard";
import React from "react";
import renderer from "react-test-renderer";

it("should match snapshot", () => {
	const wrapper = renderer.create(<UpcomingGameCard />);
	const tree = wrapper.toJSON();
	expect(tree).toMatchSnapshot();
});
