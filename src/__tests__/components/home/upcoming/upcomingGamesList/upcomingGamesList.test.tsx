import { UpcomingGamesList } from "@components/home/upcoming/upcomingGamesList/upcomingGamesList";
import React from "react";
import renderer from "react-test-renderer";

it("should match snapshot", () => {
	const wrapper = renderer.create(<UpcomingGamesList />);
	const tree = wrapper.toJSON();
	expect(tree).toMatchSnapshot();
});
